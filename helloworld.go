package main

import (
        "fmt"
        "log"
        "net/http"
        "os"
        "encoding/json"
)

type Article struct {
        Title string `json:"Title"`
        Desc string `json:"desc"`
        Content string `json:"content"`
    }
    
    type Articles []Article
    
func handler(w http.ResponseWriter, r *http.Request) {
        log.Print("Hello world received a request.")
        target := os.Getenv("TARGET")
        if target == "" {
                target = "World"
        }
        fmt.Fprintf(w, "Hello %s!\n", target)
}

func handlerUpdateJobs(w http.ResponseWriter, r *http.Request) {
	
	target := "OK"
	
	fmt.Fprintf(w, target)
}

func returnAllArticles(w http.ResponseWriter, r *http.Request){
        articles := Articles{
            Article{Title: "Hello", Desc: "Article Description", Content: "Article Content"},
            Article{Title: "Hello 2", Desc: "Article Description", Content: "Article Content"},
        }
        fmt.Println("Endpoint Hit: returnAllArticles")
    
        json.NewEncoder(w).Encode(articles)
    }

func main() {
        log.Print("Hello world sample started.")

		http.HandleFunc("/", handler)
                http.HandleFunc("/ILKWS/updateJobs", handlerUpdateJobs)
                http.HandleFunc("/all", returnAllArticles)

        port := os.Getenv("PORT")
        if port == "" {
                port = "8082"
        }

        log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}